# Hack Pair

El objetivo de este proyecto es desarrollar un sitio web open source con un 
sistema de registro reservada a una comunidad especifica (universidad, 
institucion, clase) para que cualquier desarrollador miembro de la misma 
pueda reservar horas hombre de asesoría técnica de un algún superior 
que posee un conocimiento especializado para la resolución de un problema. A 
diferencia de StackOverflow, comunidad en la que se exige que la duda sea una 
duda tecnica puntual con el fin de obtener una respuesta concreta, la duda puede 
llegar a pasar de lo puntual a una duda de mayor alcance. El experto podrá 
seleccionar entre los diferentes problemas de los miembros y asi concretar 
una hora especifa para el encuentro (ya sea real o virtual)
class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :username, :role, :name, :lastname, :knowledge
  has_many :issues
end

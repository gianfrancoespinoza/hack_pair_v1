class IssueSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :date, :incentive, :interval, :mentor 
  belongs_to :user
end

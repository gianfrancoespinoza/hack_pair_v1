class User < ApplicationRecord
	has_many :issues, dependent: :destroy
	devise :database_authenticatable
	enum role: [:student, :mentor, :admin]

	validates :email, uniqueness: true, presence: true
	validates :password, presence: true, if: "password.nil?", length: { minimum: 4 }
	validates :password, confirmation: { case_sensitive: true }
	validates :username, uniqueness: true, if: "username?", length: { in: 4..16 }
	validates :name, presence: true
	validates :lastname, presence: true
end

class Issue < ApplicationRecord
  belongs_to :user
  
  validates :user_id, presence: true
  validates :title, presence: true, length: { in: 8..60 }
  validates :description, presence: true
  validates :incentive, length: { in: 0..20 }


  def datevalid
    if date.to_date < Date.today
        update_attribute(:date, "")
        save
    end
  end

  def self.current (current_user)
    where(user: current_user)
  end
  
  def self.mentor (current_user)
    where(mentor: current_user.email)
  end
  
  def self.acepted
    where(status: true)
  end

  def self.matching
    where(status: false)
  end

  def self.no_appointment
    where(date: "")
  end
  def self.no_appointmentnil
    where(date: nil) 
  end

  def self.valid
    where.not(date: "")
  end
  def self.appointment(date,interval)
    where(date: date).where(interval: interval)
  end

end
 



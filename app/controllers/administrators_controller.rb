class AdministratorsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_role

  def index
  	@issues = Issue.all
  end

 private

 def authenticate_role
  if current_user.role != "admin"
    redirect_to dispatcher_send_to_url
  end
 end

end

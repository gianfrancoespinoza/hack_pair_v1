class StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_role

  def index
    super
  	@user = current_user
    @issues = Issue.where("user_id LIKE ?", @user.id )
    @issue = Issue.new
  end

  private

	def authenticate_role
		if current_user.role != "student"
			redirect_to dispatcher_send_to_url
		end
  end

end

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def index
    #Delete dates if date < today
    Issue.valid.each do |issue|
      issue.datevalid
    end

    #dates variables
    @dates = Date.today.beginning_of_month..Date.today.beginning_of_month + 1.year
    @month = Date.today.beginning_of_month..Date.today.end_of_month 
    @new_dates = @dates.map{ |date| date.strftime("%a %d %b") }
    @day = @month.map{|date| date}
    @init = false
    @today = Date.today
    @week = Date.today..Date.today + 1.week - 1.day
    @weekday = @week.map{|date| date}
    @hour = ["7:00am","8:00am","9:00am","10:00am","11:00am","12:00pm","1:00pm","2:00pm","3:00pm","4:00pm","5:00pm"]
  end

end
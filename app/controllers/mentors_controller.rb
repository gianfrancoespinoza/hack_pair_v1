class MentorsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_role

  def index
    super
    @user = current_user
    @issues = Issue.where("mentor LIKE ?", current_user.email )
  end


  private

	def authenticate_role
		if current_user.role != "mentor"
			redirect_to dispatcher_send_to_url
		end
	end

end

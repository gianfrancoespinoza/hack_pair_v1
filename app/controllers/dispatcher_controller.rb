class DispatcherController < ApplicationController
  before_action :authenticate_user!
  
	def send_to
		@user_role = current_user.role
		if @user_role == "admin"
			redirect_to administrators_index_url
		elsif @user_role == "mentor"
			redirect_to mentors_index_url
		else 
			redirect_to students_index_url
		end
  end
  
end

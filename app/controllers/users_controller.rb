class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_role, except: [:show, :edit, :update]

  def index
    @users = User.all.paginate(page: params[:page], per_page: 8)
    @user = User.new
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
    if (current_user.id == @user.id) || (current_user.role == 'admin')
      
    else
      respond_to do |format|
        format.html { redirect_to dispatcher_send_to_url, notice: "#{current_user.id}#{@user.id}Acceso denegado." }
      end
    end
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_url, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end

  def destroy
    if current_user.id == @user.id
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'You can not destoy yourself.' }
        format.json { head :no_content }
      end
    else
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password_confirmation, :password, :role, :username, :knowledge, :name, :lastname)
  end
  
  def authenticate_role
    if current_user.role != "admin"
      redirect_to dispatcher_send_to_url
    end
  end

end

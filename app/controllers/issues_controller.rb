class IssuesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_issue, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_role, only: [:index]
  include ApplicationHelper

   # DRAG methods
   def date_status
    @issue = Issue.find(params[:id])
    @today = Date.today
    @issue.update_attribute(:date, params[:date])
    @issue.update_attribute(:interval, params[:interval])
    if @issue.date.to_date < @today
      @issue.update_attribute(:date, "")
    end
    @issue.save
  end


  def index
    super

    @role = current_user.role
    if @role == 'admin'
      @issues = Issue.all.paginate(page: params[:page], per_page: 4)
    else
      @issues = Issue.matching.paginate(page: params[:page], per_page: 4)
    end    
  end

  def show
    @issue = Issue.find(params[:id])
    @user_issue = @issue.user.id
    @role = current_user.role
    if authenticate_student(@user_issue)
      respond_to do |format|
        format.html { redirect_to students_index_url, notice: 'Access denied' }
      end
    end
  end

  def new  
   if current_user.role != 'student'
      respond_to do |format|
        format.html { redirect_to dispatcher_send_to_url, notice: 'Students only' }
      end
    else 
      @issue = Issue.new
      @user = current_user
    end
  end

  def edit
    @user_issue = @issue.user.id
    if current_user.role == 'mentor'
      redirect_to dispatcher_send_to_url
    else
      if authenticate_student(@user_issue)
        respond_to do |format|
          format.html { redirect_to students_index_url, notice: 'Access denied' }
        end
      end
    end
  end

  def create
    @issue = Issue.new(issue_params)
    @issue.user = current_user
    respond_to do |format|
      if @issue.save
        format.html { redirect_to dispatcher_send_to_url, notice: 'Issue was successfully created.' }
        # Modals
        # format.html { redirect_back fallback_location: dispatcher_send_to_url, notice: 'Issue was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if current_user.role == 'mentor'
        if (@issue.status == true)  
          if (@issue.mentor == current_user.email)
            @issue.status = false
            @issue.mentor = nil
            @issue.save
            respond_to do |format|
              format.html { redirect_back fallback_location: @issue, notice: 'Issue cancelado'}
            end
          else
            respond_to do |format|
              format.html { redirect_back fallback_location: @issue, notice: 'Denied application'}
            end
          end
        else
          @issue.status = true
          @issue.mentor = current_user.email
          @issue.save
          respond_to do |format|
            format.html { redirect_to dispatcher_send_to_path, notice: 'Request accepted'}
          end 
       end 
    else
      respond_to do |format|
        if @issue.update(issue_params)
          format.html { redirect_to issues_path, notice: 'Updated request' }
          format.json { render :show, status: :ok, location: @issue } 
        else
          format.html { render :edit }
          format.json { render json: @issue.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def destroy
    @issue.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: issues_path, notice: 'Issue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_issue
    @issue = Issue.find(params[:id])
  end

  def issue_params
    params.require(:issue).permit(:user_id, :title, :description, :incentive, :date, :interval)
  end

  def authenticate_role
    if current_user.role == "student"
      redirect_to dispatcher_send_to_url
    end
  end

end

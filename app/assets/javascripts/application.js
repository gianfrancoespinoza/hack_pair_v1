// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require interactjs

$(document).ready(function() {
  $('#yes-drop').click(
           function() {
     $('#dragalert').toggle();
  });
});

var dragMoveListener;

dragMoveListener = function(event) {
  var target, x, y;
  target = event.target;
  x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
  y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
  
  target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
  target.setAttribute('data-x', x);
  return target.setAttribute('data-y', y);
  
};

window.dragMoveListener = dragMoveListener;

interact('*[data-draggable="true"]').draggable({
  onstart: function(event) {
        event.target.classList.remove('scroll')
      },
  inertia: true,
  autoScroll: true,
  onmove: dragMoveListener,
  restrict: {
    restriction: "#calendar",
    endOnly: true,
    elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
  },
});
 
var date = document.getElementsByTagName("date")
var $dragalert = document.getElementsByTagName("dragalert")[0] 


$(document).on('turbolinks:load', function(){  
    interact('.tap-target')
      .on('doubletap', function (event) {  
        event.preventDefault();
        event.currentTarget.attributes('data-show')
        $("#myModal").on("shown.bs.modal", function () { 
          alert('Hi');
        });
        $("#myModal").modal('show');
      })      
    interact('.acepted_value').dropzone({
      accept: '*[data-draggable="true"]',
      overlap: 0.55,
      ondragmove: function(event) {
      },
      ondropactivate: function(event) {
      },
      ondragenter: function(event) {
        event.target.classList.remove('scroll')
        event.target.classList.add('drop-target');
        event.relatedTarget.classList.add('can-drop');
      },
      ondragleave: function(event) {
        event.target.classList.remove('drop-target');
        event.relatedTarget.classList.remove('can-drop');
        return $.get(event.relatedTarget.attributes['data-url'].value, {
          date: "", 
          hour: ""
        });
      },
      ondrop: function(event) {
        let index =  event.target.attributes["index"].value
        return $.get(event.relatedTarget.attributes['data-url'].value, {
          date: date[index].attributes["date"].value, 
          interval: date[index].attributes["interval"].value
        });
      },
      ondropdeactivate: function(event) {
        event.target.classList.remove('drop-active');
        return event.target.classList.remove('drop-target');
      }
    });
  });

persistentScrollsPositions = {};


$(document).on('turbolinks:before-visit', function() {
    var y = [window.scrollY];

    persistentScrollsPositions = y
});



$(document).on('turbolinks:load', function() {
  document.documentElement.scrollTop = persistentScrollsPositions
  console.log(persistentScrollsPositions)
});
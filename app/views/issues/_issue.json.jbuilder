json.extract! issue, :id, :user_id, :title, :description, :incentive, :date, :interval, :status, :mentor, :created_at, :updated_at
json.url issue_url(issue, format: :json)

json.extract! user, :id, :email, :password, :role, :username, :knowledge, :name, :lastname, :created_at, :updated_at
json.url user_url(user, format: :json)

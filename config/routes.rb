Rails.application.routes.draw do
  devise_for :users
  
  resources :users
  resources :issues

  get 'students/index'
  get 'mentors/index'
  get 'administrators/index'
  get 'dispatcher/send_to'
  get 'issues/set_status'
  #drag
  get '/issue/:id/date_status', to: 'issues#date_status', as: :date_status


  #Api
   namespace :api do
    namespace :v1 do
      resources :users
    end
  end

  root 'dispatcher#send_to'
end

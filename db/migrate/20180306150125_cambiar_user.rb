class CambiarUser < ActiveRecord::Migration[5.1]
 def change
    rename_column :users, :password, :encrypted_password
    change_column :users, :email, :string, :default => ""
    change_column :users, :encrypted_password, :string, :default => ""
    change_column :users, :email, :string, :null => false, :default => ""
  end
  add_index :users, :email, unique: true
end

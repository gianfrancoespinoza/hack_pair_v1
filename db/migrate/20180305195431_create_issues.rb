class CreateIssues < ActiveRecord::Migration[5.1]
  def change
    create_table :issues do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.string :description
      t.string :incentive
      t.string :date, default: ""
      t.string :interval
      t.boolean :status, default: false
      t.string :mentor

      t.timestamps
    end
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Seed de Users / role:0 student, role:1 mentor, role:2 admin

User.create(email: "romer@email.com", password: "123123", role: 2, username: "RomeRRamos", name: "romer", lastname: "Ramos", knowledge: "")
User.create(email: "mariana@email.com", password: "123123", role: 2, username: "Mariale", name: "mariana", lastname: "Legonia", knowledge: "")
User.create(email: "oriele@email.com", password: "123123", role: 2, username: "Oriele", name: "Oriele", lastname: "Bermudez", knowledge: "")

User.create(email: "juca@email.com", password: "123123", role: 1, username: "Lord Juca", name: "Juan", lastname: "Manrrique", knowledge: "")
User.create(email: "carlos@email.com", password: "123123", role: 1, username: "CarlosForms", name: "Carlos", lastname: "Torrealba", knowledge: "")
User.create(email: "nelson@email", password: "123123", role: 1, username: "Nelson", name: "Nelson", lastname: "Marcano", knowledge: "")
User.create(email: "christian@email.com", password: "123123", role: 1, username: "Christian P.", name: "Christian", lastname: "Petersen", knowledge: "")
User.create(email: "jorge@email.com", password: "123123", role: 1, username: "Jorge", name: "Jorge", lastname: "Snow", knowledge: "")


User.create(email: "andres@email.com", password: "123123", role: 0, username: "AndresDeAvila", name: "Andres", lastname: "De Avila", knowledge: "")
User.create(email: "jefferson@email.com", password: "123123", role: 0, username: "Jefferson", name: "Jefferson", lastname: "Colmenares", knowledge: "")
User.create(email: "berrios@email.com", password: "123123", role: 0, username: "Jose Berrios", name: "Jose", lastname: "Berrios", knowledge: "")
User.create(email: "cesar@email.com", password: "123123", role: 0, username: "Cesar", name: "Cesar", lastname: "Mayo", knowledge: "")
User.create(email: "lucas@email.com", password: "123123", role: 0, username: "LucasPineda", name: "Lucas", lastname: "Pineda", knowledge: "")
User.create(email: "alfredo@email.com", password: "123123", role: 0, username: "Alfredo", name: "alfredo", lastname: "Velasquez", knowledge: "")
User.create(email: "alejandro@email.com", password: "123123", role: 0, username: "Alejandro", name: "Alejandro", lastname: "Maimus", knowledge: "")
User.create(email: "gustavo@email.com", password: "123123", role: 0, username: "Gustavo", name: "Gustavo", lastname: "Guerrero", knowledge: "")
User.create(email: "jesus@email.com", password: "123123", role: 0, username: "Yisus", name: "Jesus", lastname: "Beltran", knowledge: "")
User.create(email: "yanco@email.com", password: "123123", role: 0, username: "Yanco", name: "Gianfranco", lastname: "Espinoza", knowledge: "")
User.create(email: "gian@email.com", password: "123123", role: 0, username: "Insitu", name: "Gian", lastname: "Mandrioli", knowledge: "")
User.create(email: "oscar@email.com", password: "123123", role: 0, username: "Oscar", name: "Oscar", lastname: "Espinoza", knowledge: "")
User.create(email: "gabriel@email.com", password: "123123", role: 0, username: "Gabriel", name: "Gabriel", lastname: "Burgazzi", knowledge: "")
User.create(email: "manuel@email.com", password: "123123", role: 0, username: "Manuel", name: "Manuel", lastname: "Rodriguez", knowledge: "")
User.create(email: "rocco@email.com", password: "123123", role: 0, username: "Prof. Rocco", name: "Rocco", lastname: "De Fina", knowledge: "")
User.create(email: "fernanda@email.com", password: "123123", role: 0, username: "FernandaLagrage", name: "Fernanda", lastname: "Lagrange", knowledge: "")
User.create(email: "arturo@email.com", password: "123123", role: 0, username: "Arturo", name: "Arturo", lastname: "Garban", knowledge: "")
User.create(email: "mariela@email.com", password: "123123", role: 0, username: "Mariela", name: "Mariela", lastname: "Snow", knowledge: "")
User.create(email: "federico@email.com", password: "123123", role: 0, username: "Federico :v ", name: "Federico", lastname: "Snow", knowledge: "")
User.create(email: "alejandrof@email.com", password: "123123", role: 0, username: "AlejandroFactory", name: "Alejandro", lastname: "Snow", knowledge: "")
# User.create(email: "", password: "123123", role: 0, username: "", name: "", lastname: "", knowledge: "")










# Seed de Issues
Issue.create(user_id: 11, title: " Problemas de ruoting", description: "Necesito configurar rutas complejas en una apliacion web y tengo dudas en como usar enrutamiento tipo namespace, scope y shallow", incentive: "4€", interval: "7:00am", date: "2018-03-17")
Issue.create(user_id: 12, title: " Validaciones en el modelo", description: "Cada vez que edito a un usuario la aplicacion me obliga a introducir el password cada vez que quiero actualizar los cambios", incentive: "2$", interval: "9:00am", date: "2018-03-20")
Issue.create(user_id: 13, title: " Asociaciones multiples", description: "Tengo un modelado de 16 tablas con asociaciones compleajas de muchos a muchos, de 1 a muchos, de 1 a 1 y qusiera llegar a algunos datos a traves de la relacion has_many throght pero no logro acceder a los datos de la relacion uno a muchos ", incentive: "1$",          interval: "12:00pm", date: "2018-03-16")
Issue.create(user_id: 14, title: " Uso de Devise", description: "Quiero usar toda la gema Devise en una aplicacion que ya tiene un modelo User con su tabla users pero no logro configurar el modelo de para que trabaje  con Devise", incentive: "Un golfeado", interval: "1h", date: "2018-03-21")
Issue.create(user_id: 15, title: " Alarmas en modales", description: "Como poner alerts y notice en los modales", incentive: "un fuerte abrazo", interval: "1h", date: "2018-03-22")
Issue.create(user_id: 16, title: " Ajax", description: "Estoy utilizando un ajax para actualizar un campo de la vista sin necesidad de refrescar pero tengo problemas con los parametros que debo pasar", incentive: "12$", interval: "", date: "")
Issue.create(user_id: 17, title: " Como Usar Turbolinks", description: "Tengo una aplicacion estable y quiero crear un APK utilizando Turbolinks. Me gustaria que me explicaran de nuevo todo sobre Turbolink", incentive: "4€", interval: "8:00am", date: "2018-03-17")
Issue.create(user_id: 18, title: " React", description: "Tengo problemas para acceder a aulgunos atributos de los props", incentive: "Un golfeado operado y cafe grande", interval: "1h", date: "2018-03-19")
Issue.create(user_id: 19, title: " ActiveModelSerializers", description: "If I want to switch from the default attributes adapter to the json api adapter, where would I do this?", incentive: "Hearty breakfast", interval: "1h", date: "2018-03-18")
Issue.create(user_id: 20, title: " Mensajes de alertas y errores en las vistas", description: "Cuando uso Bootstrap para darle estilo a los notice y alerts de las vistas el color del fondo de bootstrap vuelve a aparecer cuando regreso a la vista donde se gener'o la alerta he intentado mover el alert del layout a otras vista pero el problema continua", incentive: "4 paquetes de harina pan", interval: "5:00pm", date: "2018-03-23")
Issue.create(user_id: 21, title: " Como usar bootstrap para mensajes flash y notice", description: "esta es la descripcion de mi issue esta es la descripcion de mi issue esta es la descripcion de mi issue esta es la descripcion de mi issue", incentive: "1$",          interval: "", date: "")
Issue.create(user_id: 22, title: " Validaciones de ActiveRecords en CREATE", description: "When writing validations I find validates :auth_token, presence: true, on: :create fails. However, validates :auth_token, presence: true, on: :after_create works. Why is this?", incentive: "12$", interval: "3:00pm", date: "2018-03-19")
Issue.create(user_id: 23, title: " Paperclip", description: "I'm getting this error when I try to upload using paperclip with my rails blogging app. Not sure what it is referring to when it says MissingRequiredValidatorError I thought that by updating … post_params and giving it :image it would be fine, as both create and update use post_params Paperclip::Errors::MissingRequiredValidatorError in PostsController#create Paperclip::Errors …", incentive: "1$", interval: "11:00am", date: "2018-03-23")

